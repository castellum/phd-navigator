var form = document.querySelector('form');
var list = document.querySelector('ul');

var doFilter = function() {
    Array.from(list.children).forEach(row => {
        row.hidden = !form[row.dataset.source].checked;
    });
}

form.addEventListener('change', doFilter);
doFilter();
