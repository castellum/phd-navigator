import argparse
import os
import re
import smtplib
import ssl
import unicodedata
from email.message import EmailMessage
from xml.sax.saxutils import escape

from markdown_it import MarkdownIt

TEMPLATE = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="default-src 'self'">
    <title>PhD Navigator</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <form>
        <h2>Please select the sources that are relevant for you</h2>
        {filters}
    </form>
    <ul aria-live="polite" class="list">
        {rows}
    </ul>
    <script src="filter.js" type="module"></script>
</body>
</html>"""

TEMPLATE_FILTER = """<label>
    <input type="checkbox" name="{slug}">
    {label}
</label>"""

TEMPLATE_ROW = """<li data-source="{source_slug}" class="list-item">
    <h2>{title}</h2>
    {description}
    <span>{source}</span>
    <a href="mailto:{contact}" class="contact">report correction</a>
</li>"""

md = MarkdownIt('commonmark', {'html': False, 'linkify': True})
md.enable('linkify')


def send_mail(subject, body):
    host = os.getenv('MAIL_HOST')
    port = os.getenv('MAIL_PORT')
    user = os.getenv('MAIL_USER')
    password = os.getenv('MAIL_PASSWORD')

    if not host:
        return

    msg = EmailMessage()
    msg.set_content(body)
    msg['Subject'] = subject
    msg['From'] = os.getenv('MAIL_FROM')
    msg['To'] = os.getenv('MAIL_TO')

    with smtplib.SMTP(host, port) as smtp:
        context = ssl.create_default_context()
        smtp.starttls(context=context)
        smtp.login(user, password)
        return smtp.send_message(msg)


def slugify(text):
    text = unicodedata.normalize('NFKD', text)
    text = text.encode('ascii', 'ignore').decode('ascii')
    text = text.strip().lower()
    text = re.sub(r'[\s_]', '-', text)
    text = re.sub(r'[^\w-]', '', text)
    return text


def unique(items):
    return list(dict.fromkeys(items))


def validate_last_entry(entries):
    if entries:
        entries[-1]['source']
        entries[-1]['priority']


def get_entries(path):
    entries = []
    contact = None
    with open(path) as fh:
        for i, line in enumerate(fh, start=1):
            try:
                if line.startswith('### '):
                    validate_last_entry(entries)
                    if contact is None:
                        raise ValueError('contact not set')
                    entries.append({
                        'title': line.removeprefix('### ').strip(),
                        'contact': contact,
                        'description': '',
                    })
                elif line.startswith('#'):
                    continue
                elif line.startswith('source:'):
                    entries[-1]['source'] = line.removeprefix('source:').strip()
                elif line.startswith('priority:'):
                    entries[-1]['priority'] = int(line.removeprefix('priority:').strip(), 10)
                elif line.startswith('contact:'):
                    contact = line.removeprefix('contact:').strip()
                elif entries:
                    entries[-1]['description'] += line
            except Exception as e:
                raise ValueError(f'error in {path}:{i}: {e}') from e
    validate_last_entry(entries)
    return entries


def render_filter(source):
    return TEMPLATE_FILTER.format(
        label=escape(source),
        slug=slugify(source),
    )


def render_row(entry):
    return TEMPLATE_ROW.format(
        title=escape(entry['title']),
        description=md.render(entry['description']),
        source=escape(entry['source']),
        source_slug=slugify(entry['source']),
        contact=escape(entry['contact']),
    )


def render(entries):
    sources = unique([e['source'] for e in entries])
    return TEMPLATE.format(
        filters='\n'.join(render_filter(s) for s in sources),
        rows='\n'.join(render_row(e) for e in entries),
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate the PhD Navigator website',
    )
    parser.add_argument('paths', nargs='+', metavar='PATH')
    args = parser.parse_args()

    try:
        entries = []
        for path in args.paths:
            entries += get_entries(path)
        print(render(sorted(entries, key=lambda e: -e['priority'])))
    except Exception as e:
        send_mail('PhD Nav Error', str(e))
        raise
