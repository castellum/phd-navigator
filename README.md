The PhD Navigator is a website that collects information for PhD students.

It is generated from files with the following structure:

```
contact:

### title

description (may contain markdown)

source:
priority:

### title
…
```

Usage:

```sh
python3 pad2html.py *.txt > index.html
```
